﻿using System;

namespace CoreOSR.Web.Public.Models
{
    public class RegisterModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string phoneNumber { get; set; }
        public int? genderId { get; set; }
        public DateTime? creationTime { get; set; }
        public bool? isActive { get; set; } = false;
    }
}
