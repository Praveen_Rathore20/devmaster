using Microsoft.AspNetCore.Mvc;
using CoreOSR.Web.Controllers;
using CoreOSR.Register;
using Abp.Application.Services.Dto;
using CoreOSR.Web.Public.Models;
using CoreOSR.Register.Dto;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using RestSharp;
using AutoMapper.Configuration;

namespace CoreOSR.Web.Public.Controllers
{
    public class HomeController : CoreOSRControllerBase
    {
        private readonly IRegisterAppService _registerAppService;
        public HomeController(IRegisterAppService registerAppService)
        {
            this._registerAppService = registerAppService;
        }
        public ActionResult Index()
        {
            return RedirectToAction("GridPage");
        }

        public ActionResult GridPage()
        {
            return View();
        }

        #region Get
        /// <summary>
        /// Get Data - Receive all the records from DB
        /// </summary>
        /// <returns></returns>
        public IActionResult GetData()
        {
            try
            {
                var data = _registerAppService.GetRegister(new GetPeopleInput() { Filter = "" });
                //var client = new RestClient("http://localhost:45776/Register/GetAllRegistrations");
                //client.Timeout = -1;
                //var request = new RestRequest(Method.GET);
                //IRestResponse response = client.Execute(request);
                //var data = response.Content;

                return Json(data.Items);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Delete
        /// <summary>
        /// Delete Entry from database based on Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Delete(int id)
        {
            try
            {
                _registerAppService.DeleteRegister(new EntityDto() { Id = id });
                //var data = new { Id = id };
                //var client = new RestClient("http://localhost:22742/Register/DeleteRegistration");
                //client.Timeout = -1;
                //var request = new RestRequest(Method.DELETE);
                //request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", JsonConvert.SerializeObject(data), ParameterType.RequestBody); ;
                //IRestResponse response = client.Execute(request);
                return Json("");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Add
        /// <summary>
        /// Add New records inside DB.
        /// </summary>
        /// <param name="posteddata"></param>
        /// <returns></returns>
        public async Task<IActionResult> Add(string posteddata)
        {
            if (!string.IsNullOrEmpty(posteddata))
            {
                RegisterModel data = JsonConvert.DeserializeObject<RegisterModel>(posteddata);
                try
                {
                    CreateRegisterInput model = new CreateRegisterInput()
                    {
                        Address = data.address,
                        CreationTime = DateTime.Now,
                        CreatorUserId = 1,
                        Email = data.email,
                        GenderId = data.genderId ?? 1,
                        IsActive = true,
                        Name = data.name,
                        PhoneNumber = data.phoneNumber
                    };

                    var id = await _registerAppService.CreateRegister(model);
                    //var client = new RestClient("http://localhost:22742/Register/CreateRegistration");
                    //client.Timeout = -1;
                    //var request = new RestRequest(Method.POST);
                    //request.AddHeader("Content-Type", "application/json");
                    //request.AddParameter("application/json", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
                    //IRestResponse response = client.Execute(request);
                    return Json(id);
                }
                catch (Exception ex)
                {
                    return Json("");
                }
            }
            return null;
        }
        #endregion

        #region Edit/Update
        /// <summary>
        /// Edit/Update records in DB
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(RegisterModel data)
        {
            try
            {
                var register = await _registerAppService.GetRegisterById(new EntityDto() { Id = data.id });
                EditRegisterInput model = new EditRegisterInput();
                model.PhoneNumber = data.phoneNumber ?? register.PhoneNumber;
                model.Address = data.address ?? register.Address;
                model.GenderId = data.genderId ?? register.GenderId;
                model.Email = data.email ?? register.Email;
                model.IsActive = data.isActive ?? data.isActive.Value;
                model.Name = data.name ?? register.Name;
                model.Id = data.id;
                model.CreationTime = register.CreationTime;
                model.CreatorUserId = register.CreatorUserId;
                await _registerAppService.EditRegister(model);


                //var client = new RestClient("http://localhost:22742/Register/EditRegistration");
                //client.Timeout = -1;
                //var request = new RestRequest(Method.PUT);
                //request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
                //IRestResponse response = client.Execute(request);
                return Json("");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Sample
        public IActionResult Sample()
        {
            return View();
        }
        #endregion

    }
}