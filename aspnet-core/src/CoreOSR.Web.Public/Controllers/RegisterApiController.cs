﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using CoreOSR.Register;
using CoreOSR.Register.Dto;
using Abp.AspNetCore.Mvc.Controllers;

namespace CoreOSR.Web.Public.Controllers
{
    [Route("Register/{action}")]
    public class RegisterApiController : AbpController
    {
        private readonly IRegisterAppService _registerAppService;
        public RegisterApiController(IRegisterAppService registerAppService)
        {
            this._registerAppService = registerAppService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllRegistrations()
        {
            var data = _registerAppService.GetRegister(new Register.Dto.GetPeopleInput() { Filter = "" });
            return Ok(data);
        }

        [HttpPost]
        public async Task<IActionResult> CreateRegistration([FromBody]CreateRegisterInput input)
        {
            var id = await _registerAppService.CreateRegister(input);
            return Ok(id);
        }

        [HttpPut]
        public async Task<IActionResult> EditRegistration([FromBody]EditRegisterInput input)
        {
            await _registerAppService.EditRegister(input);
            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> GetRegistrationById([FromBody]EntityDto input)
        {
            var reg = await _registerAppService.GetRegisterById(input);
            return Ok(reg);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteRegistration([FromBody]EntityDto input)
        {
            await _registerAppService.DeleteRegister(input);
            return Ok();
        }

    }

}