﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using CoreOSR.Context;
using CoreOSR.Register;
using CoreOSR.Register.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreOSR.Register
{
    public class RegisterAppService : CoreOSRAppServiceBase, IRegisterAppService
    {
        private readonly IRepository<RegisterTable> _registerRepository;

        public RegisterAppService(IRepository<RegisterTable> registerRepository)
        {
            _registerRepository = registerRepository;
        }

        public async Task<int> CreateRegister(CreateRegisterInput input)
        {
            var register = ObjectMapper.Map<RegisterTable>(input);
            register.CreatorUserId = 1;
            var id = GetMaxId();
            id = id + 1;
            register.Id = id;
            await _registerRepository.InsertAsync(register);
            return id;
        }

        public async Task DeleteRegister(EntityDto input)
        {
            await _registerRepository.DeleteAsync(input.Id);
        }

        public async Task EditRegister(EditRegisterInput input)
        {
            var register = _registerRepository.Get(input.Id);
            register.Name = input.Name;
            register.Address = input.Address;
            register.PhoneNumber = input.PhoneNumber;
            register.Email = input.Email;
            register.GenderId = input.GenderId;
            register.LastModificationTime = DateTime.Now;
            register.IsActive = input.IsActive;
            await _registerRepository.UpdateAsync(register);
        }

        public int GetMaxId()
        {
            using (OSRDbContext db = new OSRDbContext())
            {
                return db.Register.Select(a => a.Id).LastOrDefault();
            }
        }

        public ListResultDto<RegisterListDto> GetRegister(GetPeopleInput input)
        {
            var register = _registerRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter) ||
                         p.Email.Contains(input.Filter)
                )
                .OrderBy(p => p.Name)
                .ToList();

            return new ListResultDto<RegisterListDto>(ObjectMapper.Map<List<RegisterListDto>>(register));
        }

        public async Task<RegisterDto> GetRegisterById(EntityDto input)
        {
            var sourceRegister = await _registerRepository.GetAsync(input.Id);
            var register = ObjectMapper.Map<RegisterDto>(sourceRegister);
            return register;
        }


    }
}
