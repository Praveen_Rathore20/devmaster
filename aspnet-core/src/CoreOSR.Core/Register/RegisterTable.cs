﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CoreOSR.Register
{
    [Table("Register")]
    public class RegisterTable : FullAuditedEntity
    {
        public const int MaxNameLength = 50;
        public const int MaxAddressLength = 100;
        public const int MaxPhoneNumberLength = 100;
        public const int MaxEmailAddressLength = 255;
        [Required]
        [Key]
        public override int Id { get; set; }
        [Required]
        [MaxLength(MaxNameLength)]
        public string Name { get; set; }
        [MaxLength(MaxAddressLength)]
        public string Address { get; set; }
        [MaxLength(MaxPhoneNumberLength)]
        public string PhoneNumber { get; set; }
        [Required]
        [MaxLength(MaxEmailAddressLength)]
        public string Email { get; set; }
        [Required]
        public int GenderId { get; set; }
        public bool IsActive { get; set; }
    }
}
