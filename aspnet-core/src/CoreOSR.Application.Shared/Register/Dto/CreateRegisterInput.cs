﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CoreOSR.Register.Dto
{
    public class CreateRegisterInput
    {
        [Required]
        [MaxLength(RegisterConsts.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [MaxLength(RegisterConsts.MaxAddressLength)]
        public string Address { get; set; }

        [EmailAddress]
        [MaxLength(RegisterConsts.MaxEmailAddressLength)]
        public string Email{ get; set; }
        public string PhoneNumber { get; set; }
        public int GenderId { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsActive { get; set; } = false;
        public int Id { get; set; }
    }
}
