﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreOSR.Register.Dto
{
    public class RegisterListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int GenderId { get; set; }
        public bool IsActive { get; set; }
    }
}
