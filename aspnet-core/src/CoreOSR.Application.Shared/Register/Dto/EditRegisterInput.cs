﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreOSR.Register.Dto
{
    public class EditRegisterInput : CreateRegisterInput
    {
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }

    }
}
