﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreOSR.Register.Dto
{
    public class GetPeopleInput
    {
        public string Filter { get; set; }
    }
}
