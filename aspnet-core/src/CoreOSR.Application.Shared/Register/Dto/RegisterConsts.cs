﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreOSR.Register.Dto
{
    public class RegisterConsts
    {
        public const int MaxNameLength = 50;
        public const int MaxAddressLength = 100;
        public const int MaxEmailAddressLength = 100;
    }
}
