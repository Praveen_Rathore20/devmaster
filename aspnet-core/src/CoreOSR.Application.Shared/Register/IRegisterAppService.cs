﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using CoreOSR.Register.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CoreOSR.Register
{
    public interface IRegisterAppService: IApplicationService
    {
        ListResultDto<RegisterListDto> GetRegister(GetPeopleInput input);
        Task<int> CreateRegister(CreateRegisterInput input);
        Task EditRegister(EditRegisterInput input);
        Task<RegisterDto> GetRegisterById(EntityDto input);
        Task DeleteRegister(EntityDto input);
        int GetMaxId();
    }
}
