﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore; 
using System.Linq;
using Abp.Domain.Entities;

namespace CoreOSR.EntityFrameworkCore.Repositories
{
    class DbRepo<T> : IDbRepo<T> where T : Entity
    {
        private readonly CoreOSRDbContext context;
        private DbSet<T> entities;
        string errorMessage = string.Empty;
        public DbRepo(CoreOSRDbContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }
        public IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }
        public T Get(long id)
        {
            return entities.SingleOrDefault(s => s.Id == id);
        }
        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            context.SaveChanges();
        }
        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.SaveChanges();
        }
        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            context.SaveChanges();
        }
    }
}
